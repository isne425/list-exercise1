#include <iostream>
#include "list.h"
using namespace std;

void main()
{
	
	List mylist;

	// pushToHead fn testing
	mylist.pushToHead('k');
	mylist.pushToHead('e');
	mylist.pushToHead('n');
	mylist.print();

	// Pop fn testing
	mylist.popHead();
	mylist.print();
	mylist.popTail();
	mylist.print();
	cout << endl;
	// pushToTail fn testing
	mylist.pushToTail('e');
	mylist.pushToTail('n');
	mylist.print();

	mylist.reverse();
	mylist.print();

	mylist.search('e');
	if (true) { cout << "e is in the list." << endl; }
	else if (false) { cout << "e is not in the list." << endl; }
	

	system("pause");
}