#include <iostream>
#include "list.h"
using namespace std;

List::~List() {
	for (Node *p; !isEmpty(); ) {
		p = head->next;
		delete head;
		head = p;
	}
}

void List::pushToHead(char el)
{
	head = new Node(el, head);
	if (tail == 0)
	{
		tail = head;
	}
}
void List::pushToTail(char el)
{
	Node *newnode = new Node(el);

	if (!isEmpty())
	{
		tail->next = newnode;
		tail = newnode;
	}
	else
	{
		head = newnode;
		tail = newnode;
	}
}
char List::popHead()
{
	char el = head->data;
	Node *tmp = head;
	if (head == tail)
	{
		head = tail = 0;
	}
	else
	{
		head = head->next;
	}
	delete tmp;
	return el;
}
char List::popTail()
{
	Node *newnode = head;
	int a = tail->data;
	if (isEmpty()) { return NULL; }
	if (!isEmpty() && head != tail)
	{
		while (newnode->next->next != NULL)
		{
			newnode = newnode->next;
		}
		tail = newnode;
		newnode = tail->next;
		tail->next = NULL;
		delete newnode;
		return a;
	}
	else if (!isEmpty() && head == tail)
	{
		a = newnode->data;
		head = newnode->next;
		delete newnode;
		return a;
	}
	return NULL;
}


bool List::search(char el)
{
	Node *x, *y = head;
	x = head;
	while (true)
	{
		if (head != 0 && x->data == 'el')
		{
			return true;
		}
		else if (x != tail)
		{
			y = x;
			x = x->next;
		}
		else
		{
			break;
		}
	}
	return false;
}
void List::reverse()
{
	Node *tmp = head;
	Node *fin;
	if (head != 0)
	{
		char temp;
		Node *tmp2 = tail;
		fin = tmp2;
		if (head != tail)
		{
			while (true)
			{
				if (tmp->next == fin && fin != head)
				{
					temp = tmp->data;
					pushToTail(temp);
					fin = tmp;
					tmp = head;
				}
				else
				{
					tmp = tmp->next;
				}
				if (fin == head)
				{
					break;
				}
			}
			while (true)
			{
				if (head != tmp2)
				{
					popHead();
				}
				else
				{
					break;
				}
			}
		}
	}
}


void List::print()
{
	if (head == tail)
	{
		cout << head->data;
	}
	else
	{
		Node *tmp = head;
		while (tmp != tail)
		{
			cout << tmp->data;
			tmp = tmp->next;
		}
		cout << tmp->data << endl;
	}
}